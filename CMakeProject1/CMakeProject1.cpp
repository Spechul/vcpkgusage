﻿// CMakeProject1.cpp: определяет точку входа для приложения.
//

#include "CMakeProject1.h"
#include <sqlite3.h>
#include <rapidjson/document.h>
#include <rapidjson/rapidjson.h>
#include "func.h"
using namespace std;

int main()
{
	cout << "Hello CMake." << endl;
	cout << sqlite3_libversion() << endl;
	cout << squareRoot(5) << endl;
	return 0;
}
