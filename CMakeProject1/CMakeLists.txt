﻿# CMakeList.txt: проект CMake для CMakeProject1; включите исходный код и определения,
# укажите здесь логику для конкретного проекта.
#
cmake_minimum_required (VERSION 3.8)

find_package(unofficial-sqlite3 CONFIG REQUIRED)
set(RAPIDJSON_INCLUDE_DIRS "${PROJECT_SOURCE_DIR}/../vcpkg/packages/rapidjson_x64-windows/include")
find_path(RAPIDJSON_INCLUDE_DIRS "rapidjson/allocators.h")

# Добавьте источник в исполняемый файл этого проекта.
add_executable (CMakeProject1 "CMakeProject1.cpp" "CMakeProject1.h" "func.cpp" "func.h")

target_link_libraries(CMakeProject1 PRIVATE unofficial::sqlite3::sqlite3)
target_include_directories(CMakeProject1 PRIVATE ${RAPIDJSON_INCLUDE_DIRS})
# TODO: Добавьте тесты и целевые объекты, если это необходимо.
